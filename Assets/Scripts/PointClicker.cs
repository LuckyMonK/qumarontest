using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointClicker : MonoBehaviour
{
   
   private Camera _camera;
   [SerializeField] private CircleMovement _circle;
   private void Start()
   {
      _camera = Camera.main;
   }

   private void Update()
   {
      if (Input.GetMouseButtonUp(0))
      {
         AddPointToPath();
      }
   }

   private void AddPointToPath()
   {
      _circle.AddPoint(Input.mousePosition);
   }
}
