using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleMovement : MonoBehaviour
{
    public Queue<Vector2> clickPoints = new Queue<Vector2>();

    private Coroutine movementCoroutine;

    [SerializeField] private RectTransform _rectTransform;
    [SerializeField] private float speed;

    private const float scaling = 100f;

    [SerializeField] private AnimationCurve[] motionCurve;

    private enum MovementState
    {
        Start = 0,
        Middle = 1,
        End = 2
    }

    [SerializeField] private MovementState _movementState;

    private void Start()
    {
        MoveToDefaultPosition();
    }

    private void MoveToDefaultPosition()
    {
        _rectTransform.anchoredPosition = new Vector2(Screen.width / 2f, Screen.height / 2f);
    }

    public void AddPoint(Vector2 point)
    {
        clickPoints.Enqueue(point);

        if (movementCoroutine == null)
        {
            movementCoroutine = StartCoroutine(Movement());
        }
    }

    private Vector2 targetPosition;
    private Vector2 startPosition;
    private Vector2 direction;
    private float pathTime;
    private float timer;

    private IEnumerator Movement()
    {
        _movementState = MovementState.Start;
        while (clickPoints.Count > 0)
        {
            targetPosition = clickPoints.Dequeue();
            startPosition = _rectTransform.anchoredPosition;
            direction = (targetPosition - _rectTransform.anchoredPosition).normalized;
            pathTime = (_rectTransform.anchoredPosition - targetPosition).magnitude / speed;
            timer = 0f;


            while (timer < pathTime)
            {
                _rectTransform.anchoredPosition = TempMoveVector(timer / pathTime);
                timer += Time.deltaTime * scaling;
                yield return null;
            }

            _rectTransform.anchoredPosition = TempMoveVector(1f);

            if (clickPoints.Count > 1)
            {
                _movementState = MovementState.Middle;
            }
            else
            {
                _movementState = MovementState.End;
            }
        }

        movementCoroutine = null;
    }

    private Vector2 TempMoveVector(float progress)
    {
        return startPosition +
               direction * (motionCurve[(int) _movementState].Evaluate(progress) * speed * pathTime);
    }
}